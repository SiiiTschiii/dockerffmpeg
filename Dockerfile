FROM golang:1.12.1-alpine
RUN apk add  --no-cache ffmpeg

WORKDIR /app
COPY ./build/cutter .