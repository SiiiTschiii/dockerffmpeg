package main

import (
	"log"
	"os/exec"
)

func main() {

	log.Print("Start")

	out, err := exec.Command("ffmpeg", "-h").Output()
	log.Printf("Running command and waiting for it to finish...")
	if err != nil {
		log.Print(err)
	}
	log.Printf("Finished out: %s", out)
}
