.PHONY: bundle

bundle: prepare_output_dir service
	mv cutter/cutter build/cutter/

prepare_output_dir:
	rm -rf build
	mkdir -p build
	mkdir -p build/cutter

service: prepare_output_dir
	cd cutter; go build -o cutter
